# local redirects
> A way to quickly access all your local web server - built with vue 2.6

## Why run this on your server?
As a static site, this requires some configuration but will load very quickly and runs on any web server. I personally run this on a Raspberry Pi 3 B+ with Apache2.

## How do I configure the site?
Edit json.js objects with server data. The file in this repository is pre-loaded with sample data to show the design but is as follows:

```javascript

var serverData = [
    {
        name: "Test Server",
        checked: true
    },
    {
        name: "Test Server 2",
        checked: true
    }
]

var siteData = [
    {
        name: "Test Lamp Stack",
        addresses: ["127.0.0.1", "0.0.0.0"],
        server: "Test Server",
        web: true,
        info: "Linux Apache MySQL PHP stack",
        hidden: false,
        imageTag: "svg/globe.svg"
    },
    {
        name: "Plex Server",
        addresses: ["app.plex.tv"],
        server: "Test Server 2",
        web: true,
        info: "Plex Media Server link",
        hidden: false,
        imageTag: "svg/play-btn.svg"
    }
]


```

serverData[].name is attached to siteData[].server - this allows a handshake using vue to toggle sites on and off based on the checkbox panel on the left. The imageTag is a location in the svg/ directory with 9 preconfigured images from bootstrap.

## How can I trust this code?
This code runs entirely in the browser, and is based one Vue. I have the file set to run a local copy I downloaded, but if you would prefer you could change the script tag to Vue's cloudflare address located at https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.11/vue.min.js. You can do this by modifying line 43 of the html as:
```html
<script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.11/vue.min.js'></script>
```
Additionally, this site uses very little Javascript other than the Vue framework. This is located in script.js and is as follows:
```javascript
//define Vue application
var app = new Vue({
    // attatch to html tag
	el:"#app",
    //pull data from json.js file
	data:{
		servers: serverData,
		sites: siteData
	},
	methods:{
		//a function called when a checkbox is toggled
        showHide(passedServer){
			//check all sites to hide or show
            for(var i = 0; i < app._data.sites.length; i++){
				if(passedServer == app._data.sites[i].server){
					//make true false and false true
                    app._data.sites[i].hidden = !app._data.sites[i].hidden;
				}
			}
		}
	}
})

```
