var serverData = [
    {
        name: "Test Server",
        checked: true
    },
    {
        name: "Test Server 2",
        checked: true
    }
]

var siteData = [
    {
        name: "Test Lamp Stack",
        addresses: ["127.0.0.1", "0.0.0.0"],
        server: "Test Server",
        web: true,
        info: "Linux Apache MySQL PHP stack",
        hidden: false,
        imageTag: "svg/globe.svg"
    },
    {
        name: "Plex Server",
        addresses: ["app.plex.tv"],
        server: "Test Server 2",
        web: true,
        info: "Plex Media Server link",
        hidden: false,
        imageTag: "svg/play-btn.svg"
    }
]
